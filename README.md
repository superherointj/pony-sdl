Simple DirectMedia Layer (SDL) wrapper in Pony Language

[![pipeline status](https://gitlab.com/superherointj/pony-sdl/badges/master/pipeline.svg)](https://gitlab.com/superherointj/pony-sdl/commits/master)

[Work in Progress - This repository is a draft. It is not ready for use. Total garbage! ]

Goal:
* Avoid having to use/worry/consider C language when using SDL library.
* Offering a good working structure/pattern that hopefully avoids commons mistakes.

This goal has not been achieved yet. It is an aspiration.


Demo applications are available at:
https://gitlab.com/superherointj/pony-sdl-demos


---
MIT License