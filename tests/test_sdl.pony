use "ponytest"
use "../pony-sdl"

class iso _TestSDLVideo is UnitTest
  fun name(): String => "sdlvideo"
  fun apply(h: TestHelper) =>
    let sdl = SDLVideo(SDLFlags.init_video(), "SDLVideo Test", 640, 480)
    h.assert_eq[U32](1, 1)
