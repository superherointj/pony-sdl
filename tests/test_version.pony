use "ponytest"
use "../pony-sdl"

class iso _TestSDLVersion is UnitTest
  fun name(): String => "sdlversion"
  fun apply(h: TestHelper) =>
    var mysdlversion = SDLVersion
    @SDL_GetVersion[None](mysdlversion)
    var mysdlrevision = @SDL_GetRevision[Pointer[U8]]()
    // Debug("Major: " + mysdlversion.major.string())
    // Debug("Minor: " + mysdlversion.minor.string())
    // Debug("Patch: " + mysdlversion.patch.string())
    // Debug("Revision: " + String.from_cstring(mysdlrevision))
    // Check for SDL2
    h.assert_eq[U8](mysdlversion.major, 2)
