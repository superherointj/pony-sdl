use "ponytest"
use "../pony-sdl"

class iso _TestSDLMouse is UnitTest
  fun name(): String => "sdlmouse"
  fun apply(h: TestHelper) =>
    h.assert_eq[U8](1, 1)
