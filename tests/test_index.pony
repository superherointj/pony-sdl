use "ponytest"
use "../pony-sdl"
// use "debug"

actor Main is TestList
  new create(env: Env) =>
    PonyTest(env, this)

  new make() =>
    None

  fun tag tests(test: PonyTest) =>
    test(_TestSDLAudio)
    test(_TestSDLVideo)
    test(_TestSDLMouse)
    test(_TestSDLVersion)







////////// NOTES

/// EXAMPLE

// function SDL_LoadIMG(file: String):_SDLSurface
//     let temp_RWops = @SDL_RWFromFile(file.cstring(), "rb".cstring())
//     let temp_Surface = @IMG_LoadPNG_RW(temp_RWops)



// https://stdlib.ponylang.io/builtin-String/


////////// Video/Window should be optional!