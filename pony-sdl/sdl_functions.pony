use "lib:SDL2"

///////////////////////////////////////////////////////////////////////////////
//                            Functions Declarations                         //
///////////////////////////////////////////////////////////////////////////////

  ///////////////////////
 //       Basics      //
///////////////////////

///////////////////
// Initialization and Shutdown - SDL.h - https://wiki.libsdl.org/CategoryInit
///////////////////

  use @SDL_Init[I32](flags: U32)
  use @SDL_InitSubSystem[I32](flags: U32)
  use @SDL_Quit[None]()
  use @SDL_QuitSubSystem[None](flags: U32)
  use @SDL_SetMainReady[None]()
  use @SDL_WasInit[U32](flags: U32)
  // use @SDL_WinRTRunApp[I32](mainFunction: MainFunction, reserved: None) //DRAFT

///////////////////
// Configuration Variables - SDL_hints.h - https://wiki.libsdl.org/CategoryHints
///////////////////

    //use @SDL_AddHintCallback[None](name: Pointer[U8] tag, callback: _SDLHintCallback, userdata: Pointer[void*])
    use @SDL_ClearHints[None]()
    //use @SDL_DelHintCallback[None](name: Pointer[U8] tag, callback: _SDLHintCallback, userdata: Pointer[void*])
    use @SDL_GetHint[Pointer[U8] tag](name: Pointer[U8] tag)    //use @SDL_GetHint[const char*](name: const char*)
    use @SDL_GetHintBoolean[Bool](name: Pointer[U8] tag, default_value: Bool)
    use @SDL_SetHint[Bool](name: Pointer[U8] tag, value: Pointer[U8] tag)
    // use @SDL_SetHintWithPriority[Bool](name: Pointer[U8] tag, value: Pointer[U8] tag, priority: _SDLHintPriority tag) //SDL_bool

    // To-Do: 
    //   _SDLHintCallback is a callback with following structure:
    //      void SDL_HintCallback(void* userdata, const char* name, const char* oldValue, const char* newValue)
    // More info: https://tutorial.ponylang.io/c-ffi/callbacks.html


//////////////////
// Error Handling - SDL_error.h - http://hg.libsdl.org/SDL/file/default/include/SDL_error.h
//////////////////

  use @SDL_ClearError[None]()
  use @SDL_GetError[Pointer[U8] tag]()
  // use @SDL_SetError[I32](fmt: Pointer[U8] tag, ...) // What does "..." do here?

/////////////////
// Log Handling - SDL_log.h - https://wiki.libsdl.org/CategoryLog
/////////////////

    // SDL_Log
    // SDL_LogCritical
    // SDL_LogDebug
    // SDL_LogError
    // SDL_LogGetOutputFunction
    // SDL_LogGetPriority
    // SDL_LogInfo
    // SDL_LogMessage
    // SDL_LogMessageV
    // SDL_LogResetPriorities
    // SDL_LogSetAllPriority
    // SDL_LogSetOutputFunction
    // SDL_LogSetPriority
    // SDL_LogVerbose
    // SDL_LogWarn

/////////////
// Assertions - https://wiki.libsdl.org/CategoryAssertions
/////////////

    //     SDL_GetAssertionHandler
    //     SDL_GetAssertionReport
    //     SDL_GetDefaultAssertionHandler
    //     SDL_ResetAssertionReport
    //     SDL_SetAssertionHandler
    //     SDL_TriggerBreakpoint
    //     SDL_assert
    //     SDL_assert_paranoid
    //     SDL_assert_release

/////////////
// Querying SDL Version - SDL_revision.h - https://wiki.libsdl.org/CategoryVersion
/////////////

    use @SDL_GetRevision[Pointer[U8]]()
    use @SDL_GetRevisionNumber[I32]()
    use @SDL_GetVersion[None](ver: SDLVersion) // ???  tag????

    // MACROs are unavailable in Pony, affecting:
    //  SDL_COMPILEDVERSION, SDL_REVISION, SDL_VERSION, SDL_VERSIONNUM, SDL_VERSION_ATLEAST


    ////////////////////
   //      Video     //
  ////////////////////

//////////////////////////////////////
// Display and Window Management - SDL_video.h - https://wiki.libsdl.org/CategoryVideo
////////////////////////////////////

  use @SDL_CreateWindow[_SDLWindow](title: Pointer[U8] tag, x: I32, y: I32, w: I32, h: I32, flags: U32)
  // SDL_CreateWindowAndRenderer
  // SDL_CreateWindowFrom
  use @SDL_DestroyWindow[None](window: _SDLWindow tag)
  // SDL_DisableScreenSaver
  // SDL_EnableScreenSaver
  // SDL_GL_CreateContext
  // SDL_GL_DeleteContext
  // SDL_GL_ExtensionSupported
  // SDL_GL_GetAttribute
  // SDL_GL_GetCurrentContext
  // SDL_GL_GetCurrentWindow
  // SDL_GL_GetDrawableSize
  // SDL_GL_GetProcAddress
  // SDL_GL_GetSwapInterval
  // SDL_GL_LoadLibrary
  // SDL_GL_MakeCurrent
  // SDL_GL_ResetAttributes
  // SDL_GL_SetAttribute
  // SDL_GL_SetSwapInterval
  // SDL_GL_SwapWindow
  // SDL_GL_UnloadLibrary
  // SDL_GetClosestDisplayMode
  // SDL_GetCurrentDisplayMode
  // SDL_GetCurrentVideoDriver
  // SDL_GetDesktopDisplayMode
  // SDL_GetDisplayBounds
  // SDL_GetDisplayDPI
  // SDL_GetDisplayMode
  // SDL_GetDisplayName
  // SDL_GetDisplayUsableBounds
  // SDL_GetGrabbedWindow
  // SDL_GetNumDisplayModes
  // SDL_GetNumVideoDisplays
  // SDL_GetNumVideoDrivers
  // SDL_GetVideoDriver
  // SDL_GetWindowBordersSize
  // SDL_GetWindowBrightness
  // SDL_GetWindowData
  // SDL_GetWindowDisplayIndex
  // SDL_GetWindowDisplayMode
  // SDL_GetWindowFlags
  // SDL_GetWindowFromID
  // SDL_GetWindowGammaRamp
  // SDL_GetWindowGrab
  // SDL_GetWindowID
  // SDL_GetWindowMaximumSize
  // SDL_GetWindowMinimumSize
  // SDL_GetWindowOpacity
  // SDL_GetWindowPixelFormat
  // SDL_GetWindowPosition
  // SDL_GetWindowSize
  // SDL_GetWindowSurface
  // SDL_GetWindowTitle
  // SDL_GetWindowWMInfo
  // SDL_HideWindow
  // SDL_IsScreenSaverEnabled
  // SDL_MaximizeWindow
  // SDL_MinimizeWindow
  // SDL_RaiseWindow
  // SDL_RestoreWindow
  // SDL_SetWindowBordered
  // SDL_SetWindowBrightness
  // SDL_SetWindowData
  // SDL_SetWindowDisplayMode
  use @SDL_SetWindowFullscreen[I32](window: _SDLWindow tag, flags: U32)
  // SDL_SetWindowGammaRamp
  // SDL_SetWindowGrab
  // SDL_SetWindowHitTest
  // SDL_SetWindowIcon
  // SDL_SetWindowInputFocus
  // SDL_SetWindowMaximumSize
  // SDL_SetWindowMinimumSize
  // SDL_SetWindowModalFor
  // SDL_SetWindowOpacity
  // SDL_SetWindowPosition
  // SDL_SetWindowResizable
  // SDL_SetWindowSize
  use @SDL_SetWindowTitle[None](window: _SDLWindow tag, title: Pointer[U8] tag)
  // SDL_ShowMessageBox
  // SDL_ShowSimpleMessageBox
  // SDL_ShowWindow
  // SDL_UpdateWindowSurface
  // SDL_UpdateWindowSurfaceRects
  // SDL_VideoInit
  // SDL_VideoQuit

/////////////////////
// 2D Accelerated Rendering - SDL_render.h - http://hg.libsdl.org/SDL/file/default/include/SDL_render.h
/////////////////////

    // SDL_ComposeCustomBlendMode
    use @SDL_CreateRenderer[_SDLRenderer tag](window: _SDLWindow tag, index: I32, flags: U32)
    // SDL_CreateSoftwareRenderer
    // SDL_CreateTexture
    use @SDL_CreateTextureFromSurface[_SDLTexture tag](renderer: _SDLRenderer tag, surface: _SDLSurface tag)
    // SDL_CreateWindowAndRenderer
    use @SDL_DestroyRenderer[None](renderer: _SDLRenderer tag)
    use @SDL_DestroyTexture[None](texture: _SDLTexture tag)
    // SDL_GL_BindTexture
    // SDL_GL_UnbindTexture
    // SDL_GetNumRenderDrivers
    // SDL_GetRenderDrawBlendMode
    // SDL_GetRenderDrawColor
    // SDL_GetRenderDriverInfo
    // SDL_GetRenderTarget
    // SDL_GetRenderer
    // SDL_GetRendererInfo
    // SDL_GetRendererOutputSize
    // SDL_GetTextureAlphaMod
    // SDL_GetTextureBlendMode
    // SDL_GetTextureColorMod
    // SDL_LockTexture
    use @SDL_QueryTexture[I32](texture: _SDLTexture tag, format: Pointer[U32] , access: Pointer[I32], w: Pointer[I32], h: Pointer[I32])
    use @SDL_RenderClear[I32](renderer: _SDLRenderer tag)
    use @SDL_RenderCopy[I32](renderer: _SDLRenderer tag, texture: _SDLTexture tag, srcrect: NullablePointer[_SDLRect val] tag, dstrect: NullablePointer[_SDLRect val] tag)
    use @SDL_RenderCopyEx[I32](renderer: _SDLRenderer tag, texture: _SDLTexture tag, srcrect: NullablePointer[_SDLRect val], dstrect: NullablePointer[_SDLRect val], angle: F64, center: NullablePointer[_SDLPoint val], flip: U32) // falta testar @SDL_RenderCopyEx
    use @SDL_RenderDrawLine[I32](renderer: _SDLRenderer tag, x1: I32, y1: I32, x2: I32, y2: I32)
    use @SDL_RenderDrawLines[I32](renderer: _SDLRenderer tag, points: Array[_SDLPoint val] tag , count: I32) // ??? _SDLPoint tag???? or val??
    // SDL_RenderDrawPoint
    // SDL_RenderDrawPoints
    // SDL_RenderDrawRect
    // SDL_RenderDrawRects
    use @SDL_RenderFillRect[I32](renderer: _SDLRenderer tag, rect: NullablePointer[_SDLRect val] tag)
    // SDL_RenderFillRects
    // SDL_RenderGetClipRect
    // SDL_RenderGetIntegerScale
    // SDL_RenderGetLogicalSize
    // SDL_RenderGetScale
    // SDL_RenderGetViewport
    // SDL_RenderIsClipEnabled
    use @SDL_RenderPresent[None](renderer: _SDLRenderer tag)
    // SDL_RenderReadPixels
    // SDL_RenderSetClipRect
    // SDL_RenderSetIntegerScale
    use @SDL_RenderSetLogicalSize[I32](renderer: _SDLRenderer tag, w: I32, h: I32)
    // SDL_RenderSetScale
    // SDL_RenderSetViewport
    // SDL_RenderTargetSupported
    // SDL_SetRenderDrawBlendMode
    use @SDL_SetRenderDrawColor[I32](renderer: _SDLRenderer tag, r: U8, g: U8, b: U8, a: U8)
    // SDL_SetRenderTarget
    use @SDL_SetTextureAlphaMod[I32](texture: _SDLTexture tag, alpha: U8)
    // SDL_SetTextureBlendMode
    // SDL_SetTextureColorMod
    // SDL_UnlockTexture
    // SDL_UpdateTexture
    // SDL_UpdateYUVTexture

////////////////
// Pixel Formats and Conversion Routines - SDL_pixels.h - https://wiki.libsdl.org/CategoryPixels
////////////////

    // SDL_AllocFormat
    // SDL_AllocPalette
    // SDL_CalculateGammaRamp
    // SDL_FreeFormat
    // SDL_FreePalette
    // SDL_GetPixelFormatName
    // SDL_GetRGB
    // SDL_GetRGBA
    use @SDL_MapRGB[U32](format: _SDLPixelFormat tag, r: U8, g: U8, b: U8 )
    // SDL_MapRGBA
    // SDL_MasksToPixelFormatEnum
    // SDL_PixelFormatEnumToMasks
    // SDL_SetPaletteColors
    // SDL_SetPixelFormatPalette

////////////////
// Rectangle Functions - SDL_rect.h - https://wiki.libsdl.org/CategoryRect
////////////////

    // SDL_EnclosePoints
    // SDL_HasIntersection
    // SDL_IntersectRect
    // SDL_IntersectRectAndLine
    // SDL_PointInRect
    // SDL_RectEmpty
    // SDL_RectEquals
    // SDL_UnionRect

////////////////
/// Surface Creation and Simple Drawing - SDL_surface.h
////////////////

  // SDL_BlitScaled
  // SDL_BlitSurface
  // SDL_ConvertPixels
  // SDL_ConvertSurface
  // SDL_ConvertSurfaceFormat
  // SDL_CreateRGBSurface
  // SDL_CreateRGBSurfaceFrom
  // SDL_CreateRGBSurfaceWithFormat
  // SDL_CreateRGBSurfaceWithFormatFrom
  // SDL_FillRect
  // SDL_FillRects
  use @SDL_FreeSurface[None](surface: _SDLSurface tag)
  // SDL_GetClipRect
  // SDL_GetColorKey
  // SDL_GetSurfaceAlphaMod
  // SDL_GetSurfaceBlendMode
  // SDL_GetSurfaceColorMod
  use @SDL_LoadBMP[_SDLSurface](file: Pointer[U8] tag)
  use @SDL_LoadBMP_RW[_SDLSurface](src: _SDLRWops tag, freesrc: I32)
  // SDL_LockSurface
  // SDL_LowerBlit
  // SDL_LowerBlitScaled
  // SDL_MUSTLOCK
  // SDL_SaveBMP
  // SDL_SaveBMP_RW
  // SDL_SetClipRect
  use @SDL_SetColorKey[I32](surface: _SDLSurface tag, flag: I32, key: U32)
  use @SDL_SetSurfaceAlphaMod[I32](surface: _SDLSurface tag, alpha: U8) //tag
  // SDL_SetSurfaceBlendMode
  // SDL_SetSurfaceColorMod
  // SDL_SetSurfacePalette
  // SDL_SetSurfaceRLE
  // SDL_UnlockSurface

///////////////////////////////////////////////////////////
// Surface Creation and Simple Drawing - SDL_surface.h - http://hg.libsdl.org/SDL/file/default/include/SDL_surface.h
///////////////////////////////////////////////////////////

  // Structures
  //     SDL_Surface

  // Functions
  //     SDL_BlitScaled
  //     SDL_BlitSurface
  //     SDL_ConvertPixels
  //     SDL_ConvertSurface
  //     SDL_ConvertSurfaceFormat
  //     SDL_CreateRGBSurface
  //     SDL_CreateRGBSurfaceFrom
  //     SDL_CreateRGBSurfaceWithFormat
  //     SDL_CreateRGBSurfaceWithFormatFrom
  //     SDL_FillRect
  //     SDL_FillRects
  //     SDL_FreeSurface
  //     SDL_GetClipRect
  //     SDL_GetColorKey
  //     SDL_GetSurfaceAlphaMod
  //     SDL_GetSurfaceBlendMode
  //     SDL_GetSurfaceColorMod
  //     SDL_LoadBMP
  //     SDL_LoadBMP_RW
  //     SDL_LockSurface
  //     SDL_LowerBlit
  //     SDL_LowerBlitScaled
  //     SDL_MUSTLOCK
  //     SDL_SaveBMP
  //     SDL_SaveBMP_RW
  //     SDL_SetClipRect
  //     SDL_SetColorKey
  //     SDL_SetSurfaceAlphaMod
  //     SDL_SetSurfaceBlendMode
  //     SDL_SetSurfaceColorMod
  //     SDL_SetSurfacePalette
  //     SDL_SetSurfaceRLE
  //     SDL_UnlockSurface

///////////////////////////////////////////////////////////
// Platform-specific Window Management - SDL_syswm.h - http://hg.libsdl.org/SDL/file/default/include/SDL_syswm.h
///////////////////////////////////////////////////////////

  // Enumerations
  //     SDL_SYSWM_TYPE

  // Structures
  //     SDL_SysWMinfo
  //     SDL_SysWMmsg

  // Functions
  //     SDL_GetWindowWMInfo

///////////////////////////////////////////////////////////
// Clipboard Handling - SDL_syswm.h - http://hg.libsdl.org/SDL/file/default/include/SDL_clipboard.h
///////////////////////////////////////////////////////////

    // SDL_GetClipboardText
    // SDL_HasClipboardText
    // SDL_SetClipboardText

///////////////////////////////////////////////////////////
// Vulkan Support - SDL_vulkan.h - http://hg.libsdl.org/SDL/file/default/include/SDL_vulkan.h
///////////////////////////////////////////////////////////

    // SDL_Vulkan_CreateSurface
    // SDL_Vulkan_GetDrawableSize
    // SDL_Vulkan_GetInstanceExtensions
    // SDL_Vulkan_GetVkInstanceProcAddr
    // SDL_Vulkan_LoadLibrary
    // SDL_Vulkan_UnloadLibrary


  //////////////////
 // Input Events //
//////////////////

///////////
// Event Handling - SDL_events.h - https://wiki.libsdl.org/CategoryEvents
///////////

  // SDL_AddEventWatch
  // SDL_DelEventWatch
  // SDL_EventState
  // SDL_FilterEvents
  // SDL_FlushEvent
  // SDL_FlushEvents
  // SDL_GetEventFilter
  // SDL_GetEventState
  // SDL_GetNumTouchDevices
  // SDL_GetNumTouchFingers
  // SDL_GetTouchDevice
  // SDL_GetTouchFinger
  // SDL_HasEvent
  // SDL_HasEvents
  // SDL_LoadDollarTemplates
  // SDL_PeepEvents
  use @SDL_PollEvent[I32](event: NullablePointer[_SDLKeyboardEvent] ) // To-Fix: int SDL_PollEvent(SDL_Event* event) // tag ????
  // SDL_PumpEvents
  // SDL_PushEvent
  // SDL_QuitRequested
  // SDL_RecordGesture
  // SDL_RegisterEvents
  // SDL_SaveAllDollarTemplates
  // SDL_SaveDollarTemplate
  // SDL_SetEventFilter
  // SDL_WaitEvent
  // SDL_WaitEventTimeout

///////////
// Keyboard Support - SDL_keyboard.h, SDL_keycode.h, SDL_scancode.h - https://wiki.libsdl.org/CategoryKeyboard
///////////

    //     SDL_GetKeyFromName
    //     SDL_GetKeyFromScancode
    //     SDL_GetKeyName
    //     SDL_GetKeyboardFocus
    //     SDL_GetKeyboardState
    //     SDL_GetModState
    //     SDL_GetScancodeFromKey
    //     SDL_GetScancodeFromName
    //     SDL_GetScancodeName
    //     SDL_HasScreenKeyboardSupport
    //     SDL_IsScreenKeyboardShown
    //     SDL_IsTextInputActive
    //     SDL_SetModState
    //     SDL_SetTextInputRect
    //     SDL_StartTextInput
    //     SDL_StopTextInput

///////////
// Mouse Support - SDL_mouse.h - https://wiki.libsdl.org/CategoryMouse
///////////

  use @SDL_CaptureMouse[I32](enabled: Bool)                                            // int SDL_CaptureMouse(SDL_bool enabled)
  use @SDL_CreateColorCursor[_SDLCursor tag](surface: _SDLSurface tag, hot_x: I32, hot_y: I32) // SDL_Cursor* SDL_CreateColorCursor(SDL_Surface* surface, int hot_x, int hot_y)
  use @SDL_CreateCursor[_SDLCursor tag](data: Pointer[U8] tag, mask: Pointer[U8] tag, w: I32, h: I32, hot_x: I32, hot_y: I32) // SDL_Cursor* SDL_CreateCursor(const Uint8* data, const Uint8* mask, int w, int h, int hot_x, int hot_y)
  use @SDL_CreateSystemCursor[_SDLCursor tag](id: _SDLSystemCursor tag)                         // SDL_Cursor* SDL_CreateSystemCursor(SDL_SystemCursor id)
  use @SDL_FreeCursor[None](cursor: _SDLCursor tag)                                         // void SDL_FreeCursor(SDL_Cursor* cursor)
  use @SDL_GetCursor[_SDLCursor tag]()                                                      // SDL_Cursor* SDL_GetCursor(void)
  use @SDL_GetDefaultCursor[_SDLCursor tag]()                                               // SDL_Cursor* SDL_GetDefaultCursor(void)
  use @SDL_GetGlobalMouseState[U32](x: Pointer[I32] tag, y: Pointer[I32] tag)                   // Uint32 SDL_GetGlobalMouseState(int* x, int* y)
  use @SDL_GetMouseFocus[_SDLWindow tag]()                                                  // SDL_Window* SDL_GetMouseFocus(void)
  use @SDL_GetMouseState[U32](x: Pointer[I32] tag, y: Pointer[I32] tag)                         // Uint32 SDL_GetMouseState(int* x, int* y)
  use @SDL_GetRelativeMouseMode[Bool]()                                                 // SDL_bool SDL_GetRelativeMouseMode(void)
  use @SDL_GetRelativeMouseState[U32](x: Pointer[I32] tag, y: Pointer[I32] tag)                 // Uint32 SDL_GetRelativeMouseState(int* x, int* y)
  use @SDL_SetCursor[None](cursor: _SDLCursor tag)                                          // void SDL_SetCursor(SDL_Cursor* cursor)
  use @SDL_SetRelativeMouseMode[I32](enabled: Bool)                                     // int SDL_SetRelativeMouseMode(SDL_bool enabled)
  use @SDL_ShowCursor[I32](toggle: I32)                                                 // int SDL_ShowCursor(int toggle)
  use @SDL_WarpMouseGlobal[I32](x: I32, y: I32)                                         // int SDL_WarpMouseGlobal(int x, int y)
  use @SDL_WarpMouseInWindow[None](window: _SDLWindow tag, x: I32, y: I32)                  // void SDL_WarpMouseInWindow(SDL_Window* window, int x, int y)

///////////
// Joystick Support - SDL_joystick.h - https://wiki.libsdl.org/CategoryJoystick
///////////

  //     SDL_JoystickClose
  //     SDL_JoystickCurrentPowerLevel
  //     SDL_JoystickEventState
  //     SDL_JoystickFromInstanceID
  //     SDL_JoystickGetAttached
  //     SDL_JoystickGetAxis
  //     SDL_JoystickGetBall
  //     SDL_JoystickGetButton
  //     SDL_JoystickGetDeviceGUID
  //     SDL_JoystickGetGUID
  //     SDL_JoystickGetGUIDFromString
  //     SDL_JoystickGetGUIDString
  //     SDL_JoystickGetHat
  //     SDL_JoystickInstanceID
  //     SDL_JoystickName
  //     SDL_JoystickNameForIndex
  //     SDL_JoystickNumAxes
  //     SDL_JoystickNumBalls
  //     SDL_JoystickNumButtons
  //     SDL_JoystickNumHats
  //     SDL_JoystickOpen
  //     SDL_JoystickUpdate
  //     SDL_NumJoysticks

///////////
// Game Controller Support - SDL_gamecontroller.h - https://wiki.libsdl.org/CategoryGameController
///////////

  //     SDL_GameControllerAddMapping
  //     SDL_GameControllerAddMappingsFromFile
  //     SDL_GameControllerAddMappingsFromRW
  //     SDL_GameControllerClose
  //     SDL_GameControllerEventState
  //     SDL_GameControllerFromInstanceID
  //     SDL_GameControllerGetAttached
  //     SDL_GameControllerGetAxis
  //     SDL_GameControllerGetAxisFromString
  //     SDL_GameControllerGetBindForAxis
  //     SDL_GameControllerGetBindForButton
  //     SDL_GameControllerGetButton
  //     SDL_GameControllerGetButtonFromString
  //     SDL_GameControllerGetJoystick
  //     SDL_GameControllerGetStringForAxis
  //     SDL_GameControllerGetStringForButton
  //     SDL_GameControllerMapping
  //     SDL_GameControllerMappingForGUID
  //     SDL_GameControllerName
  //     SDL_GameControllerNameForIndex
  //     SDL_GameControllerOpen
  //     SDL_GameControllerUpdate
  //     SDL_IsGameController


    ////////////////////
   // Force Feedback //
  ////////////////////

  ////////
  // Force Feedback Support - http://hg.libsdl.org/SDL/file/default/include/SDL_haptic.h - https://wiki.libsdl.org/CategoryForceFeedback
  ////////

    // SDL_HapticClose
    // SDL_HapticDestroyEffect
    // SDL_HapticEffectSupported
    // SDL_HapticGetEffectStatus
    // SDL_HapticIndex
    // SDL_HapticName
    // SDL_HapticNewEffect
    // SDL_HapticNumAxes
    // SDL_HapticNumEffects
    // SDL_HapticNumEffectsPlaying
    // SDL_HapticOpen
    // SDL_HapticOpenFromJoystick
    // SDL_HapticOpenFromMouse
    // SDL_HapticOpened
    // SDL_HapticPause
    // SDL_HapticQuery
    // SDL_HapticRumbleInit
    // SDL_HapticRumblePlay
    // SDL_HapticRumbleStop
    // SDL_HapticRumbleSupported
    // SDL_HapticRunEffect
    // SDL_HapticSetAutocenter
    // SDL_HapticSetGain
    // SDL_HapticStopAll
    // SDL_HapticStopEffect
    // SDL_HapticUnpause
    // SDL_HapticUpdateEffect
    // SDL_JoystickIsHaptic
    // SDL_MouseIsHaptic
    // SDL_NumHaptics


  //////////////////
 //     Audio    //
//////////////////

//////////////
// Audio Device Management and Audio Playback - SDL_audio.h - https://wiki.libsdl.org/CategoryAudio
//////////////

    use @SDL_AudioInit[I32](driver_name: Pointer[U8] tag)  // int SDL_AudioInit(const char* driver_name)
    use @SDL_AudioQuit[None]()  // void SDL_AudioQuit(void)

    // use @SDL_BuildAudioCVT[I32](cvt: Pointer[_SDLAudioCVT], src_format: _SDLAudioFormat, src_channels: U8, src_rate: I32, dst_format: _SDLAudioFormat, dst_channels: U8, dst_rate: I32) 
        // To-Fix: Enumeration _SDLAudioFormat
        // int SDL_BuildAudioCVT(SDL_AudioCVT* cvt, SDL_AudioFormat src_format, Uint8 src_channels, int src_rate, SDL_AudioFormat dst_format, Uint8 dst_channels, int dst_rate)

    // use @SDL_ClearQueuedAudio[None](dev: _SDLAudioDeviceID) 
        // To-Fix: _SDLAudioDeviceID
        // void SDL_ClearQueuedAudio(SDL_AudioDeviceID dev) 

    use @SDL_CloseAudio[None]() //void SDL_CloseAudio(void)

    // use @SDL_CloseAudioDevice[None](dev: _SDLAudioDeviceID) 
        // To-Fix: _SDLAudioDeviceID
        // void SDL_CloseAudioDevice(SDL_AudioDeviceID dev)

    use @SDL_ConvertAudio[I32](cvt: Pointer[_SDLAudioCVT] tag) // int SDL_ConvertAudio(SDL_AudioCVT* cvt)

    // use @SDL_DequeueAudio[U32](dev: _SDLAudioDeviceID, data: Pointer[U8], len: U32)
        // To-Fix: _SDLAudioDeviceID    
        // Uint32 SDL_DequeueAudio(SDL_AudioDeviceID dev, void* data, Uint32 len)

    use @SDL_FreeWAV[None](audio_buf: Pointer[U8] tag)  // void SDL_FreeWAV(Uint8* audio_buf)
    use @SDL_GetAudioDeviceName[Pointer[U8]](index: I32, iscapture: I32) // const char* SDL_GetAudioDeviceName(int index, int iscapture)

    // use @SDL_GetAudioDeviceStatus[_SDLAudioStatus](dev: _SDLAudioDeviceID)
        // To-Fix: _SDLAudioDeviceID
        // SDL_AudioStatus SDL_GetAudioDeviceStatus(SDL_AudioDeviceID dev)

    use @SDL_GetAudioDriver[Pointer[U8] tag](index: I32) // const char* SDL_GetAudioDriver(int index)

    // use @SDL_GetAudioStatus[_SDLAudioStatus]()
        // To-Fix: _SDLAudioStatus
        // SDL_AudioStatus SDL_GetAudioStatus(void)

    use @SDL_GetCurrentAudioDriver[Pointer[U8] tag]() // const char* SDL_GetCurrentAudioDriver(void)
    // use @SDL_GetNumAudioDevices[]() //
    // use @SDL_GetNumAudioDrivers[]() //
    // use @SDL_GetQueuedAudioSize[]() //
    use @SDL_LoadWAV[_SDLAudioSpec](file: Pointer[U8] tag, spec: _SDLAudioSpec tag, audio_buf: Pointer[Pointer[U8]] tag, audio_len: Pointer[U32] tag) // SDL_AudioSpec* SDL_LoadWAV(const char* file, SDL_AudioSpec* spec, Uint8** audio_buf, Uint32* audio_len)
    use @SDL_LoadWAV_RW[_SDLAudioSpec](src: _SDLRWops tag, freesrc: I32, spec: _SDLAudioSpec tag, audio_bug: Pointer[Pointer[U8] tag] tag, audio_len: Pointer[U32]) // SDL_AudioSpec* SDL_LoadWAV_RW(SDL_RWops* src, int freesrc, SDL_AudioSpec* spec, Uint8** audio_buf, Uint32* audio_len)
    // use @SDL_LockAudio[]() //
    // use @SDL_LockAudioDevice[]() //
    // use @SDL_MixAudio[]() //
    // use @SDL_MixAudioFormat[]() //
    // use @SDL_OpenAudio[]() //
    // use @SDL_OpenAudioDevice[]() //
    // use @SDL_PauseAudio[]() //
    // use @SDL_PauseAudioDevice[]() //
    // use @SDL_QueueAudio[]() //
    // use @SDL_UnlockAudio[]() //
    // use @SDL_UnlockAudioDevice[]() //


  /////////////
 // Threads //
/////////////

///////////
// Thread Management - SDL_thread.h
///////////

    // SDL_CreateThread
    // SDL_DetachThread
    // SDL_GetThreadID
    // SDL_GetThreadName
    // SDL_SetThreadPriority
    // SDL_TLSCreate
    // SDL_TLSGet
    // SDL_TLSSet
    // SDL_ThreadID
    // SDL_WaitThread

///////////
// Thread Synchronization Primitives - SDL_mutex.h - https://wiki.libsdl.org/CategoryMutex
///////////

    // SDL_CondBroadcast
    // SDL_CondSignal
    // SDL_CondWait
    // SDL_CondWaitTimeout
    // SDL_CreateCond
    // SDL_CreateMutex
    // SDL_CreateSemaphore
    // SDL_DestroyCond
    // SDL_DestroyMutex
    // SDL_DestroySemaphore
    // SDL_LockMutex
    // SDL_SemPost
    // SDL_SemTryWait
    // SDL_SemValue
    // SDL_SemWait
    // SDL_SemWaitTimeout
    // SDL_TryLockMutex
    // SDL_UnlockMutex

//////////
// Atomic Operations - SDL_atomic.h 
//////////

    // SDL_AtomicAdd
    // SDL_AtomicCAS
    // SDL_AtomicCASPtr
    // SDL_AtomicDecRef
    // SDL_AtomicGet
    // SDL_AtomicGetPtr
    // SDL_AtomicIncRef
    // SDL_AtomicLock
    // SDL_AtomicSet
    // SDL_AtomicSetPtr
    // SDL_AtomicTryLock
    // SDL_AtomicUnlock
    // SDL_CompilerBarrier

  //////////////////////
 //      Timers      //
//////////////////////

//////////////////////////////////////
// Timer Support - SDL_timer.h - https://wiki.libsdl.org/CategoryTimer
///////////////////////////////////////

    // SDL_AddTimer
    // SDL_Delay
    // SDL_GetPerformanceCounter
    // SDL_GetPerformanceFrequency
    // SDL_GetTicks
    // SDL_RemoveTimer
    // SDL_TICKS_PASSED


  //////////////////////
 // File Abstraction //
//////////////////////

/////////////////
// Filesystem Paths - SDL_filesystem.h - https://wiki.libsdl.org/CategoryFilesystem
/////////////////

  // SDL_GetBasePath
  // SDL_GetPrefPath

/////////////////
// File I/O Abstraction - SDL_rwops.h - https://wiki.libsdl.org/CategoryIO
/////////////////

  // SDL_AllocRW
  // SDL_FreeRW
  // SDL_RWFromConstMem
  // SDL_RWFromFP
  use @SDL_RWFromFile[_SDLRWops](file: Pointer[U8] tag, mode: Pointer[U8] tag)
  // SDL_RWFromMem
  use @SDL_RWclose[None](context: _SDLRWops tag)
  // SDL_RWread
  // SDL_RWseek
  // SDL_RWsize
  // SDL_RWtell
  // SDL_RWwrite
  // SDL_ReadBE16
  // SDL_ReadBE32
  // SDL_ReadBE64
  // SDL_ReadLE16
  // SDL_ReadLE32
  // SDL_ReadLE64
  // SDL_ReadU8
  // SDL_WriteBE16
  // SDL_WriteBE32
  // SDL_WriteBE64
  // SDL_WriteLE16
  // SDL_WriteLE32
  // SDL_WriteLE64
  // SDL_WriteU8


  ///////////////////////////
 // Shared Object Support //
///////////////////////////

//////////////////////////
// Shared Object Loading and Function Lookup - SDL_loadso.h - http://hg.libsdl.org/SDL/file/default/include/SDL_loadso.h
//////////////////////////

  // SDL_LoadFunction
  // SDL_LoadObject
  // SDL_UnloadObject

  /////////////////////////////////////
 //   Platform and CPU Information  //
/////////////////////////////////////

///////////////
// Platform Detection  - SDL_platform.h - http://hg.libsdl.org/SDL/file/default/include/SDL_platform.h
///////////////

  use @SDL_GetPlatform[Pointer[U8] tag]() // const char* SDL_GetPlatform(void)

///////////////
// CPU Feature Detection - SDL_cpuinfo.h - http://hg.libsdl.org/SDL/file/default/include/SDL_cpuinfo.h
///////////////

    // SDL_GetCPUCacheLineSize
    // SDL_GetCPUCount
    // SDL_GetSystemRAM
    // SDL_Has3DNow
    // SDL_HasAVX
    // SDL_HasAVX2
    // SDL_HasAltiVec
    // SDL_HasMMX
    // SDL_HasRDTSC
    // SDL_HasSSE
    // SDL_HasSSE2
    // SDL_HasSSE3
    // SDL_HasSSE41
    // SDL_HasSSE42

///////////////
// Byte Order and Byte Swapping - SDL_endian.h - http://hg.libsdl.org/SDL/file/default/include/SDL_endian.h
///////////////

    // SDL_Swap16
    // SDL_Swap32
    // SDL_Swap64
    // SDL_SwapBE16
    // SDL_SwapBE32
    // SDL_SwapBE64
    // SDL_SwapFloat
    // SDL_SwapFloatBE
    // SDL_SwapFloatLE
    // SDL_SwapLE16
    // SDL_SwapLE32
    // SDL_SwapLE64

///////////////
// Bit Manipulation - SDL_bits.h - http://hg.libsdl.org/SDL/file/default/include/SDL_bits.h
///////////////

  use @SDL_MostSignificantBitIndex32[I32](x: U32) // int SDL_MostSignificantBitIndex32(Uint32 x)

  //////////////////////////
 //   Power Management   // https://wiki.libsdl.org/CategoryPower
//////////////////////////

////////////////////////////
// Power Management Status - SDL_power.h - http://hg.libsdl.org/SDL/file/default/include/SDL_power.h
////////////////////////////

  // Enumerations
    // SDL_PowerState

  // Functions
    // SDL_PowerState SDL_GetPowerInfo(int* secs, int* pct)

  ////////////
 // Additional Functionality
//////////////

////////////////////////////////////////
// Platform-specific Functionality - SDL_system.h - https://wiki.libsdl.org/CategorySystem
////////////////////////////////////////

// Enumerations
//     SDL_WinRT_Path

// Functions
//     SDL_AndroidGetActivity
//     SDL_AndroidGetExternalStoragePath
//     SDL_AndroidGetExternalStorageState
//     SDL_AndroidGetInternalStoragePath
//     SDL_AndroidGetJNIEnv
//     SDL_DXGIGetOutputInfo
//     SDL_Direct3D9GetAdapterIndex
//     SDL_RenderGetD3D9Device
//     SDL_SetWindowsMessageHook
//     SDL_WinRTGetFSPathUNICODE
//     SDL_WinRTGetFSPathUTF8
//     SDL_iPhoneSetAnimationCallback
//     SDL_iPhoneSetEventPump

////////////////////////////////////////
// Standard Library Functionality - SDL_stdinc.h - https://wiki.libsdl.org/CategoryStandard
////////////////////////////////////////

// Enumerations
//     SDL_bool

// primitive SDLTrue // SDL_TRUE = 1
// primitive SDLFalse // SDL_FALSE = 0

// type SDLBool is (SDLTrue | SDLFalse)

// Functions
//     SDL_acos
