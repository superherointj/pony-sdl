// https://www.libsdl.org/projects/SDL_image/docs/

use "lib:SDL2_image"

  /////////////
 // Defines //
/////////////

    // let IMG_MAJOR_VERSION = 1 // SDL_image library major number at compilation time 
    // let IMG_MINOR_VERSION = 2 // SDL_image library minor number at compilation time 
    // let IMG_PATCHLEVEL = 8 //  SDL_image library patch level at compilation time 
    // let IMG_INIT_JPG = 1 // IMG_Init JPG image format support flag 
    // let IMG_INIT_PNG = 2 // IMG_Init PNG image format support flag 
    // let IMG_INIT_TIF = 4 // IMG_Init TIF image format support flag 

  /////////////
 // General //   These functions query, initialize, and cleanup the SDL_image library.
/////////////

    // IMG_Linked_Version	  	// Get version number
    use @IMG_Init[I32](flags: I32) // Initialize SDL_image
    use @IMG_Quit[None]() // Cleanup SDL_image

  /////////////
 // Loading //
/////////////

    //Automagic Loading
        // IMG_Load	  	//Load from a file
        use @IMG_Load[_SDLSurface](file: Pointer[U8] tag) // Warning: Broken!! WHY?
        // IMG_Load_RW	  	//Load using a SDL_RWop
        // IMG_LoadTyped_RW //Load more format specifically with a SDL_RWop

    //Specific Loaders
        // IMG_LoadBMP_RW	  	// Load BMP using a SDL_RWop
        // IMG_LoadCUR_RW	  	// Load CUR using a SDL_RWop
        // IMG_LoadGIF_RW	  	// Load GIF using a SDL_RWop
        // IMG_LoadICO_RW	  	// Load ICO using a SDL_RWop
        // IMG_LoadJPG_RW	  	// Load JPG using a SDL_RWop
        // IMG_LoadLBM_RW	  	// Load LBM using a SDL_RWop
        // IMG_LoadPCX_RW	  	// Load PCX using a SDL_RWop
        use @IMG_LoadPNG_RW[_SDLSurface](file: _SDLRWops tag)
        // IMG_LoadPNM_RW	  	// Load PNM using a SDL_RWop
        // IMG_LoadTGA_RW	  	// Load TGA using a SDL_RWop
        // IMG_LoadTIF_RW	  	// Load TIF using a SDL_RWop
        // IMG_LoadXCF_RW	  	// Load XCF using a SDL_RWop
        // IMG_LoadXPM_RW	  	// Load XPM using a SDL_RWop
        // IMG_LoadXV_RW	  	// Load XV using a SDL_RWop

    //Array Loaders
        // IMG_ReadXPMFromArray	  	// Load XPM from compiled XPM data


    // Require SDL2_image - https://hg.libsdl.org/SDL_image/file/0f9c3eeabc11/SDL_image.h
    use @IMG_LoadTexture[_SDLTexture tag](renderer: _SDLRenderer tag, file: Pointer[U8] tag) //  extern DECLSPEC SDL_Texture * SDLCALL IMG_LoadTexture(SDL_Renderer *renderer, const char *file);
    //  extern DECLSPEC SDL_Texture * SDLCALL IMG_LoadTexture_RW(SDL_Renderer *renderer, SDL_RWops *src, int freesrc);
    //  extern DECLSPEC SDL_Texture * SDLCALL IMG_LoadTextureTyped_RW(SDL_Renderer *renderer, SDL_RWops *src, int freesrc, const char *type);

  ////////////
 //  Info  // These functions are tests for specific file formats. They also show if the format is supported in the linked SDL_image library, assuming you have a valid image of that type.
////////////

    // IMG_isBMP	  	Test for valid, supportted BMP file
    // IMG_isCUR	  	Test for valid, supportted CUR file
    // IMG_isGIF	  	Test for valid, supportted GIF file
    // IMG_isICO	  	Test for valid, supportted ICO file
    // IMG_isJPG	  	Test for valid, supportted JPG file
    // IMG_isLBM	  	Test for valid, supportted LBM file
    // IMG_isPCX	  	Test for valid, supportted PCX file
    // IMG_isPNG	  	Test for valid, supportted PNG file
    // IMG_isPNM	  	Test for valid, supportted PNM file
    // IMG_isTIF	  	Test for valid, supportted TIF file
    // IMG_isXCF	  	Test for valid, supportted XCF file
    // IMG_isXPM	  	Test for valid, supportted XPM file
    // IMG_isXV	  	    Test for valid, supportted XV file

    ////////////
   // Errors //  These functions are used for error status strings that should help the user and developer understand why a function failed.
  ////////////

    // IMG_SetError	  	Set the current error string
    // IMG_GetError	  	Get the current error string

//////////////// SDL2_image

// To-Do: Make SDL_LoadBMP work because it does not.
//        SDL_Surface* SDL_LoadBMP(const char* file)





// https://www.willusher.io/sdl2%20tutorials/2013/08/18/lesson-3-sdl-extension-libraries