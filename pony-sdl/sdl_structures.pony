///////////////////////////////////////////////////////////////////////////////
//                           Enumerations, Structures                        //
///////////////////////////////////////////////////////////////////////////////

  ///////////////////////
 //       Basics      //
///////////////////////

///////////////////
// Configuration Variables - SDL_hints.h - https://wiki.libsdl.org/CategoryHints
///////////////////

  // Enumerations
    // primitive SDLHintDefault
    // primitive SDLHintNormal
    // primitive SDLHintOverride
    // type _SDLHintPriority is (SDLHintDefault | SDLHintNormal | SDLHintOverride)

/////////////
// Assertions - https://wiki.libsdl.org/CategoryAssertions
/////////////

    // Enumerations
    //     SDL_AssertState
    // Structures
    //     SDL_assert_data

/////////////
// Querying SDL Version - SDL_revision.h - https://wiki.libsdl.org/CategoryVersion
/////////////

    struct SDLVersion
      var major: U8 = 0
      var minor: U8 = 0
      var patch: U8 = 0


    ////////////////////
   //      Video     //
  ////////////////////

//////////////////////////////////////
// Display and Window Management - SDL_video.h - https://wiki.libsdl.org/CategoryVideo
////////////////////////////////////

  // Enumerations
    // SDL_BlendMode
    // SDL_GLattr
    // SDL_GLcontextFlag
    // SDL_GLprofile
    // SDL_HitTestResult
    // SDL_MessageBoxButtonFlags
    // SDL_MessageBoxColorType
    // SDL_MessageBoxFlags
    // SDL_WindowEventID
    // SDL_WindowFlags

  // Structures
    // SDL_DisplayMode
    // SDL_MessageBoxButtonData
    // SDL_MessageBoxColor
    // SDL_MessageBoxColorScheme
    // SDL_MessageBoxData
    // SDL_WindowEvent

/////////////////////
// 2D Accelerated Rendering - SDL_render.h - https://wiki.libsdl.org/CategoryRender - http://hg.libsdl.org/SDL/file/default/include/SDL_render.h
/////////////////////

  // Enumerations
      // SDL_BlendFactor -- DRAFT -- Not available
      // SDL_BlendOperation -- DRAFT -- Not available

      // SDL_RendererFlags - Flags used when creating a rendering context
          // typedef enum
          //   {
          //     SDL_RENDERER_SOFTWARE = 0x00000001,         // The renderer is a software fallback
          //     SDL_RENDERER_ACCELERATED = 0x00000002,      // The renderer uses hardware acceleration
          //     SDL_RENDERER_PRESENTVSYNC = 0x00000004,     // Present is synchronized with the refresh rate
          //     SDL_RENDERER_TARGETTEXTURE = 0x00000008     // The renderer supports rendering to texture
          //   } SDL_RendererFlags;

      // SDL_RendererFlip - Flip constants for SDL_RenderCopyEx
          // typedef enum
          // {
          //   SDL_FLIP_NONE = 0x00000000,     // < Do not flip
          //   SDL_FLIP_HORIZONTAL = 0x00000001,    // flip horizontally
          //   SDL_FLIP_VERTICAL = 0x00000002     // flip vertically
          // } SDL_RendererFlip;

      // SDL_TextureAccess - The access pattern allowed for a texture.
          // typedef enum
          // {
          //   SDL_TEXTUREACCESS_STATIC,    // Changes rarely, not lockable
          //   SDL_TEXTUREACCESS_STREAMING, // Changes frequently, lockable
          //   SDL_TEXTUREACCESS_TARGET     // Texture can be used as a render target
          // } SDL_TextureAccess;

      // SDL_TextureModulate - The texture channel modulation used in SDL_RenderCopy().
          // typedef enum
          // {
          //   SDL_TEXTUREMODULATE_NONE = 0x00000000,     // No modulation
          //   SDL_TEXTUREMODULATE_COLOR = 0x00000001,    // srcC = srcC * color
          //   SDL_TEXTUREMODULATE_ALPHA = 0x00000002     // srcA = srcA * alpha
          // } SDL_TextureModulate;


  // Structures

    // SDL_Renderer - A structure representing rendering state
        // struct SDL_Renderer;
        // typedef struct SDL_Renderer SDL_Renderer;
      primitive _SDLRenderer

    // SDL_RendererInfo - Information on the capabilities of a render driver or context.
        // typedef struct SDL_RendererInfo
        //   {
        //     const char *name;           // The name of the renderer
        //     Uint32 flags;               // Supported ::SDL_RendererFlags
        //     Uint32 num_texture_formats; // The number of available texture formats
        //     Uint32 texture_formats[16]; // The available texture formats
        //     int max_texture_width;      // The maximum texture width
        //     int max_texture_height;     // The maximum texture height
        //   } SDL_RendererInfo;
      struct _SDLRendererInfo
        // var name: Pointer[U8] tag //const char* - To-Fix: ERROR HERE!!! => "field left undefined in constructor"
        // var flags: U32 = 0 //Uint32
        // var num_texture_formats: U32 = 0 //Uint32
        // var texture_formats: U32 = 0 //Uint32[16]
        // var max_texture_width: I32 = 0
        // var max_texture_height: I32 = 0 //int

    // SDL_Texture - An efficient driver-specific representation of pixel data
        // struct SDL_Texture;
        // typedef struct SDL_Texture SDL_Texture;
      primitive _SDLTexture


////////////////
// Pixel Formats and Conversion Routines - SDL_pixels.h - https://wiki.libsdl.org/CategoryPixels
////////////////

  // Enumerations
      // SDL_PixelFormatEnum

  // Structures
      // SDL_Color
      // SDL_Palette
      // SDL_PixelFormat

  primitive _SDLColor
  primitive _SDLPalette

  struct _SDLPixelFormat
    var format: U32 = 0
    var palette: _SDLPalette = _SDLPalette
    var bitsPerPixel: U8 = 0
    var bytesPerPixel: U8 = 0
    var rmask: U32 = 0
    var gmask: U32 = 0
    var bmask: U32 = 0
    var amask: U32 = 0
    var rloss: U8 = 0
    var gloss: U8 = 0
    var bloss: U8 = 0
    var aloss: U8 = 0
    var rshift: U8 = 0
    var gshift: U8 = 0
    var bshift: U8 = 0
    var ashift: U8 = 0
    var refcount: I32 = 0
    var next: _SDLPixelFormat = _SDLPixelFormat
    new create() => compile_intrinsic
    // fun apply(): this->A ? => compile_intrinsic

////////////////
// Rectangle Functions - SDL_rect.h - https://wiki.libsdl.org/CategoryRect
////////////////

  // Structures
      // SDL_Point
      // SDL_Rect

  struct _SDLPoint
    var x: I32 = 0
    var y: I32 = 0
    new create(x1: I32, y1: I32) =>
      x = x1
      y = y1

  struct _SDLRect
    var x: I32 
    var y: I32 
    var w: I32 
    var h: I32 
    new val create(x1: I32 = 0, y1: I32 = 0, w1: I32 = 0, h1: I32 = 0) =>
      x = x1
      y = y1
      w = w1
      h = h1

////////////////
/// Surface Creation and Simple Drawing - SDL_surface.h
////////////////

  // Structures
      // SDL_Surface

    primitive _SDLSurfacePixelsPointer
    primitive _SDLSurfaceUserData
    primitive _SDLSurfaceLockData

  struct _SDLSurface
    var flags: U32 = 0
    var format: _SDLPixelFormat  = _SDLPixelFormat
    var w: I32 = 0
    var h: I32 = 0
    var pitch: I32 = 0
    var pixels: _SDLSurfacePixelsPointer = _SDLSurfacePixelsPointer
    var userdata: _SDLSurfaceUserData = _SDLSurfaceUserData
    var locked: I32 = 0
    var lock_data: _SDLSurfaceLockData = _SDLSurfaceLockData
    var clip_rect: _SDLRect val = _SDLRect
    var map: _SDLBlitMap = _SDLBlitMap
    var refcount: I32 = 0
    // // new create(x1: I32, y1: I32, w1: I32, h1: I32) =>

////////////////////
// Platform-specific Window Management - SDL_syswm.h - https://wiki.libsdl.org/CategorySWM
//////////////////

  // Enumerations
  //   SDL_SYSWM_TYPE

  // Structures
  //   SDL_SysWMinfo
  //   SDL_SysWMmsg



  //////////////////////
 // File Abstraction //
//////////////////////

/////////////////
// File I/O Abstraction - SDL_rwops.h - https://wiki.libsdl.org/CategoryIO
/////////////////

  // Structures
  primitive _SDLRWops // SDL_RWops

  //////////////////
 // Input Events //
//////////////////

///////////
// Event Handling - SDL_events.h - https://wiki.libsdl.org/CategoryEvents
///////////

  // Enumerations
      // SDL_EventType
      // SDL_WindowEventID

  // Structures
      // SDL_AudioDeviceEvent
      // SDL_ControllerAxisEvent
      // SDL_ControllerButtonEvent
      // SDL_ControllerDeviceEvent
      // SDL_DollarGestureEvent
      // SDL_DropEvent
      // SDL_Event
      // SDL_Finger
      // SDL_JoyAxisEvent
      // SDL_JoyBallEvent
      // SDL_JoyButtonEvent
      // SDL_JoyDeviceEvent
      // SDL_JoyHatEvent
      // SDL_KeyboardEvent
      // SDL_MouseButtonEvent
      // SDL_MouseMotionEvent
      // SDL_MouseWheelEvent
      // SDL_MultiGestureEvent
      // SDL_QuitEvent
      // SDL_SysWMEvent
      // SDL_TextEditingEvent
      // SDL_TextInputEvent
      // SDL_TouchFingerEvent
      // SDL_UserEvent
      // SDL_WindowEvent

      // Total 56 bytes
      struct _SDLKeyboardEvent
        // 30 bytes
        var type1: U32 = 0
        var timestamp: U32 = 0
        var windowID: U32 = 0
        var state: U8 = 0
        var repeat1: U8 = 0
        var padding2: U8 = 0
        var padding3: U8 = 0
        var scancode: U32 = 0
        var sym: U32 = 0
        var mod: U16 = 0
        var unused: U32 = 0
        // 26 bytes
        var pad1: U64 = 0
        var pad2: U64 = 0
        var pad3: U64 = 0
        var pad4: U16 = 0

///////////
// Keyboard Support - SDL_keyboard.h, SDL_keycode.h, SDL_scancode.h - https://wiki.libsdl.org/CategoryKeyboard
///////////

    // Enumerations
    //     SDL_Keycode
    //     SDL_Keymod
    //     SDL_Scancode
    // Structures
    //     SDL_Keysym

////////////
// Mouse
////////////

primitive _SDLCursor // NO idea from where it comes from! :S
primitive _SDLSystemCursor

///////////
// Joystick Support - SDL_joystick.h - https://wiki.libsdl.org/CategoryJoystick
///////////

  // Enumerations
  //     SDL_JoystickPowerLevel

///////////
// Game Controller Support - SDL_gamecontroller.h - https://wiki.libsdl.org/CategoryGameController
///////////

  // Enumerations
  //     SDL_GameControllerAxis
  //     SDL_GameControllerButton


    ////////////////////
   // Force Feedback //
  ////////////////////

  /////////////////
  // Force Feedback Support - http://hg.libsdl.org/SDL/file/default/include/SDL_haptic.h - https://wiki.libsdl.org/CategoryForceFeedback
  /////////////////

    // Structures
        // SDL_HapticCondition
        // SDL_HapticConstant
        // SDL_HapticCustom
        // SDL_HapticDirection
        // SDL_HapticEffect
        // SDL_HapticLeftRight
        // SDL_HapticPeriodic
        // SDL_HapticRamp


  //////////////////
 //     Audio    //
//////////////////

//////////////
// Audio Device Management and Audio Playback - SDL_audio.h - https://wiki.libsdl.org/CategoryAudio
//////////////

    // Enumerations
    //     SDL_AudioFormat
    //     SDL_AudioStatus
            // SDL_AUDIO_STOPPED // audio device is stopped
            // SDL_AUDIO_PLAYING // audio device is playing
            // SDL_AUDIO_PAUSED  // audio device is paused

    // primitive SDLAudioStopped
    // primitive SDLAudioPlaying
    // primitive SDLAudioPaused
    // type _SDLAudioStatus is (SDLAudioStopped | SDLAudioPlaying | SDLAudioPaused)


    // Structures    
    primitive _SDLAudioCVT    // SDL_AudioCVT
    primitive _SDLAudioSpec   // SDL_AudioSpec
    primitive _SDLAudioStream // SDL_AudioStream
  

  /////////////////
 //   Threads   //
/////////////////

///////////
// Thread Management - SDL_thread.h
///////////

  // Enumerations
      // SDL_ThreadPriority

//////////
// Atomic Operations - SDL_atomic.h 
//////////

  // Structures
      // SDL_atomic_t

// SDL_blit.h

  // Blit mapping definition
  // typedef struct SDL_BlitMap
  // {
  //   SDL_Surface *dst;
  //   int identity;
  //   SDL_blit blit;
  //   void *data;
  //   SDL_BlitInfo info; 
  //   /* the version count matches the destination; mismatch indicates an invalid mapping */
  //   Uint32 dst_palette_version;
  //   Uint32 src_palette_version;
  // } SDL_BlitMap;

  primitive _SDLBlitMap // SDL_blit.h

// To-Do: Where is SDLWindow located?
primitive _SDLWindow