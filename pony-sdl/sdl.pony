use "collections"

///////////////////////////////////////////////////////////
//////////////// Pony - Wrapper Implementation ////////////
///////////////////////////////////////////////////////////

/////////////////// VIDEO

primitive SDLFlags
  fun init_video(): U32 => 0x00000020
  fun window_shown(): U32 => 0x00000004
  fun renderer_accelerated(): U32 => 0x00000002
  fun renderer_presentvsync(): U32 => 0x00000004
  fun flip_none(): U32 => 0x0
  fun flip_horizontal(): U32 => 0x1
  fun flip_vertical(): U32 => 0x2

/////////////////// EVENTS

primitive _SDLEvents
  fun quit(): U32 => 0x100
  fun keydown(): U32 => 0x300
  fun keyup(): U32 => 0x301

primitive SDLKeyCodes
  fun escape(): U32 => 27
  fun space(): U32 => 32
  fun left(): U32 => (1 << 30) + 80
  fun right(): U32 => (1 << 30) + 79
  fun down(): U32 => (1 << 30) + 81
  fun up(): U32 => (1 << 30) + 82

class SDLKeyUp
  let sym: U32
  new iso create(sym': U32) =>
    sym = sym'

class SDLKeyDown
  let sym: U32
  new iso create(sym': U32) =>
    sym = sym'

class SDLQuit
  new iso create() =>
    None

type SDLEvent is (SDLQuit | SDLKeyUp | SDLKeyDown)


///////////////////

class SDLTexture
  let texture: _SDLTexture tag
  let format: U32
  let w: I32
  let h: I32
  new create(texture': _SDLTexture tag, format': U32, w': I32, h': I32) =>
    texture = texture'
    format = format'
    w = w'
    h = h'


class SDLWindow
  var window: _SDLWindow tag
  new create(title: String box, x: I32 = -1, y: I32 = -1, w: I32 = 640, h: I32 = 480, flags: U32 = SDLFlags.window_shown()) =>
    window = @SDL_CreateWindow(title.cstring(), x, y, w, h, flags)
  fun ref destroy() =>
    @SDL_DestroyWindow(window)


class SDLRect
  embed rect: _SDLRect val
  new val create(x1: I32, y1: I32, w1: I32, h1: I32) =>
    rect = _SDLRect.create(x1, y1, w1, h1)


class SDLRenderer
  var renderer: _SDLRenderer tag

  new create(window: SDLWindow) =>
    renderer = @SDL_CreateRenderer(window.window, -1, SDLFlags.renderer_accelerated())

  fun ref destroy() =>
    @SDL_DestroyRenderer(renderer)

  fun ref clear() =>
    @SDL_RenderClear(renderer)

  fun ref present() =>
     @SDL_RenderPresent(renderer)

  fun ref set_draw_color(r: U8, g: U8, b: U8, a: U8 = 255) =>
    @SDL_SetRenderDrawColor(renderer, r, g, b, a)

  fun ref fill_rect(rect: (SDLRect val | None)) =>
    match rect
      | None => @SDL_RenderFillRect(renderer, NullablePointer[_SDLRect val].none())
      | let r: SDLRect val => @SDL_RenderFillRect(renderer, NullablePointer[_SDLRect val](r.rect))
    end

  //////////////////
  /// To-Fix: I could not check if it is "null" unless I use "Pointer[A], so I wonder what I should be doing about this!"
  fun ref load_texture(file: String box): SDLTexture => //?
    let texture: _SDLTexture tag = @IMG_LoadTexture(renderer, file.cstring())
    
    // if (NullablePointer[_SDLTexture](texture)).is_null() then
    //   error
    // end
    var w: I32 = 0
    var h: I32 = 0
    var format: U32 = 0
    var access: I32 = 0
    @SDL_QueryTexture(texture, addressof format, addressof access, addressof w, addressof h)
    SDLTexture(texture, format, w, h)

  fun ref draw_texture(texture: SDLTexture, rect: SDLRect val) =>
    @SDL_RenderCopy(renderer, texture.texture,
      NullablePointer[_SDLRect val](_SDLRect(0, 0, texture.w, texture.h)),
      NullablePointer[_SDLRect val](rect.rect))

  fun ref draw_texture_flip(texture: SDLTexture, rect: SDLRect val, flip: U32) =>
    @SDL_RenderCopyEx(renderer, texture.texture,
      NullablePointer[_SDLRect val](_SDLRect(0, 0, texture.w, texture.h)),
      NullablePointer[_SDLRect val](rect.rect),
      0.0,
      NullablePointer[_SDLPoint val].none(),
      flip)


class SDLVideo
  let window: SDLWindow
  let renderer: SDLRenderer
  embed textures: Map[I32, SDLTexture] = Map[I32, SDLTexture]

  new create(flags: U32, title: String val, w: I32 = 640, h: I32 = 480) =>
    @SDL_Init(flags)
    window = SDLWindow(title, 100, 100, w, h)
    renderer = SDLRenderer(window)

  fun ref clear() =>
    renderer.clear()

  fun ref set_draw_color(r: U8, g: U8, b: U8, a: U8 = 255) =>
    renderer.set_draw_color(r, g, b, a)

  fun ref fill_rect(rect: (SDLRect val | None) = None) =>
    renderer.fill_rect(rect)

  fun ref draw_texture(id: I32, rect: SDLRect val, flip: U32 = SDLFlags.flip_none()) =>
    try
      let tex = textures(id)?
      if flip != SDLFlags.flip_none() then
        renderer.draw_texture_flip(tex, rect, flip)
      else
        renderer.draw_texture(tex, rect)
      end
    end

  fun ref present() =>
    renderer.present()

  fun ref dispose() =>
    renderer.destroy()
    window.destroy()

  fun ref load_texture(file: String val, id: I32) =>
    if not textures.contains(id) then
      // try
        textures(id) = renderer.load_texture(file) //?
      // end
    end

  fun ref poll_events(): Array[SDLEvent] iso^ =>
    let events: Array[SDLEvent] iso = []
    var e: _SDLKeyboardEvent ref = _SDLKeyboardEvent
    var rc: I32 = 0
    rc = @SDL_PollEvent(NullablePointer[_SDLKeyboardEvent](e))
    while rc != 0 do
      match e.type1
        | _SDLEvents.quit() => events.push(SDLQuit)
        | _SDLEvents.keydown() => if e.repeat1 == 0 then events.push(SDLKeyDown(e.sym)) end
        | _SDLEvents.keyup() => if e.repeat1 == 0 then events.push(SDLKeyUp(e.sym)) end
      end
      rc = @SDL_PollEvent(NullablePointer[_SDLKeyboardEvent](e))
    end
    consume events