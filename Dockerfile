FROM superherointj/ponylang-archlinux-sdl as ponyapp

RUN mkdir /app/pony-sdl /app/tests/
COPY pony-sdl/* /app/pony-sdl/
COPY tests/* /app/tests/
COPY Makefile /app/
RUN make test